def pytest_addoption(parser):
    parser.addoption("--apiurl", action="store", default="http://localhost:8000/")

def pytest_generate_tests(metafunc):
    option_value = metafunc.config.option.apiurl
    if 'apiurl' in metafunc.fixturenames and option_value is not None:
        metafunc.parametrize("apiurl", [option_value])