Examples
========

Iterating through every SourceFile in every WordPress-Release
--------------

In this example we access every SourceFile in every WordPress-Release and print the md5-hash.

.. code-block:: python

	from pprint import pprint
	from weasel_client import APIClient

	def iterate_sourcefiles():
	    # Replace "abcdefg" with a valid API Token. Optional: Replace the URL with an address of a alternative WEASEL API installation.
	    client = APIClient("abcdefg", "https://weasel.cs.uni-bonn.de/")

	    # iterate through every WordPress-Release
	    for release in client.technology("WordPress").releases():

	        # iterate through ReleaseSourcefile-Entries
	        for fileReference in release.filelist():
	            pprint(fileReference.sourcefile().hash_md5())

As shown we create an :class:`~weasel_client.APIClient` providing a token. We then use this client to traverse
through the API just like browsing the web rendering of the API interface.