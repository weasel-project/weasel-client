Basics
======

`weasel_client` offers different functions and classes to access the data provided by the WEASEL API.
See also :doc:`Examples <examples>` for usage instructions.

The APIClient
----------------

The APIClient class provides a direct mapping of the WEASEL API endpoints. For every endpoint it offers a
function to access the provided data.
Every function returns either a list of or a single resource object the user can work with.

See also :class:`~weasel_client.APIClient` for further documentation of the APIClient class.

The Resource Classes
----------------------

For every set of attributes the API offers the `weasel_client` provides a resource class to hold the fetched data.
The data can then be acessed via get methods.

Whereever the API-fetched structure contains a reference to another API view the resource class provides a method to
access the referenced data.

See also :doc:`Examples <examples>` for usage instructions or the resource class documentations (f.e. :class:`~weasel_client.Release`)
for further documentation.