WEASEL Data Sources
===================

`weasel-client` is a client for the WEASEL API.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

        Basics <basics>
        Examples <examples>
        API Documentation <api>
