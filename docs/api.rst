API Documentation
=================

APIClient
-----------------
.. autoclass:: weasel_client.APIClient
	:members:
	:undoc-members:

Technology
-----------------
.. autoclass:: weasel_client.Technology
	:members:
	:undoc-members:


Release
-----------------
.. autoclass:: weasel_client.Release
	:members:
	:undoc-members:


Vulnerability
-----------------
.. autoclass:: weasel_client.Vulnerability
	:members:
	:undoc-members:

SourceFile
-----------------
.. autoclass:: weasel_client.SourceFile
	:members:
	:undoc-members:

ReleaseCounter
_________________
.. autoclass:: weasel_client.ReleaseCounter
	:members:
	:undoc-members:

DateStatistics
-----------------
.. autoclass:: weasel-client.DateStatistics
	:members:
	:undoc-members:

ReleaseSourcefile
-----------------
.. autoclass:: weasel-client.ReleaseSourcefile
	:members:
	:undoc-members:
