from .api_integration_test import APIIntegrationTest
from weasel_client import APIClient


class APIClientTestCase(APIIntegrationTest):
    def test_connection_working(self):
        api_client = self.client
        self.assertTrue(api_client.connected())
