from pprint import pprint
import unittest
import requests

from weasel_client import APIClient


class APIIntegrationTest(unittest.TestCase):
    client: APIClient

    def setUp(self):
        apiurl = "http://api-server:8000/api/"
        tokenrequest = requests.post(apiurl + "authgen/", data={"username": "root", "password": "test"})
        if not tokenrequest.status_code == 200:
            raise Exception("No API-Token", "API token could not be fetched!")
        token = tokenrequest.json()['token']
        self.client = APIClient(token=token, url=apiurl)