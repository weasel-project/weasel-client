import os
import re

from setuptools import find_packages, setup

version_file = os.path.join(
	os.path.dirname(__file__),
	'weasel_client',
	'__version__.py'
)

with open(version_file, 'r') as fp:
	m = re.search(
		r"^__version__ = ['\"]([^'\"]*)['\"]",
		fp.read(),
		re.MULTILINE
	)
	version = m.groups(1)[0]

setup(
	name='weasel_client',
	version=version,
	author='Lennart Haas',
	license='MIT',
	packages=find_packages(include=['weasel_client']),
	install_requires=[
		'requests==2.25.0'
	],
	setup_requires=['pytest-runner'],
	tests_require=[
		'pytest==4.4.1',
		'httmock==1.4.0'
	],
	test_suite='tests',
)