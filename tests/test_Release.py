import unittest
from weasel_client import APIClient, Release
from httmock import all_requests, urlmatch, HTTMock, response
from .request_binds import RequestBinds


class ReleaseTestCase(unittest.TestCase):
	_client: APIClient
	_release: Release

	def setUp(self) -> None:
		with HTTMock(RequestBinds.mock_release_detail, RequestBinds.mock_technology_list, RequestBinds.mock_root):
			self._client = APIClient(token="abcdefg", url="http://localhost:8000/api/")
			self._release = Release.from_id(api_client=self._client, primary_key=1)

	def test_Release_from_id(self):
		with HTTMock(RequestBinds.mock_release_detail, RequestBinds.mock_technology_list):
			self.assertIsNotNone(Release.from_id(api_client=self._client, primary_key=1))

	def test_Release_from_false_id(self):
		with HTTMock(RequestBinds.mock_notfound):
			self.assertIsNone(Release.from_id(api_client=self._client, primary_key=-1))

	def test_filelist(self):
		with HTTMock(RequestBinds.mock_sourcefile_list_page2, RequestBinds.mock_sourcefile_list):
			for file in self._release.filelist():
				self.assertIsNotNone(file)

	def test_installations(self):
		with HTTMock(RequestBinds.mock_release_installations_page2, RequestBinds.mock_release_installations):
			for installation in self._release.installations():
				self.assertIsNotNone(installation)

	def test_vulnerabilities(self):
		with HTTMock(RequestBinds.mock_release_vulnerabilities_page2, RequestBinds.mock_release_vulnerabilities):
			for vuln in self._release.vulnerabilities():
				self.assertIsNotNone(vuln)


if __name__ == '__main__':
	unittest.main()
