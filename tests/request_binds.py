from httmock import all_requests, urlmatch, HTTMock, response
from pprint import pprint
from urllib.parse import SplitResult

class RequestBinds:
    default_headers = {'content-type': 'application/json'}
    mock_scheme = "http"
    mock_netloc = r"localhost:8000"
    mock_basepath = r"/api/"

    @staticmethod
    @urlmatch(scheme=mock_scheme, netloc=mock_netloc, path=mock_basepath)
    def mock_connection_wrongtoken(url, request):
        return response(401, request=request)

    @staticmethod
    @urlmatch(scheme=mock_scheme, netloc=mock_netloc, path=mock_basepath)
    def mock_root(url, request):
        content = {
            "releases": "http://localhost:8000/api/releases/",
            "technologies": "http://localhost:8000/api/technologies/",
            "vulnerabilities": "http://localhost:8000/api/vulnerability/",
            "installations": "http://localhost:8000/api/installations/"
        }
        return response(200, content=content, headers=RequestBinds.default_headers, request=request)

    @staticmethod
    @urlmatch(scheme=mock_scheme, netloc=mock_netloc, path=mock_basepath + "technologies/")
    def mock_technologies(url, request):
        content = [
            {
                "primary_key": 1,
                "tech_name": "WordPress",
                "releases": "http://localhost/api/releases/WordPress/",
                "updates": "http://localhost/api/technologies/1/updates/"
            }
        ]
        return response(200, content, headers=RequestBinds.default_headers, request=request)

    @staticmethod
    @urlmatch(scheme=mock_scheme, netloc=mock_netloc, path=mock_basepath + "releases/")
    def mock_release_list(url, request):
        return RequestBinds.__release_list(url=url, request=request)

    @staticmethod
    @urlmatch(scheme=mock_scheme, netloc=mock_netloc, path=mock_basepath + "releases/", query="page=2")
    def mock_release_list_page2(url, request):
        return RequestBinds.__release_list_page2(url=url, request=request)

    @staticmethod
    @urlmatch(scheme=mock_scheme, netloc=mock_netloc, path=mock_basepath + "releases/1/")
    def mock_release_detail(url, request):
        content = {
                    "url": "http://localhost:8000/api/releases/1/",
                    "primary_key": 1,
                    "technology": "WordPress",
                    "version": "5.3.0",
                    "prev_release": None,
                    "published": "2005-05-10T00:00:00Z",
                    "zipfile": "http://localhost:8000/api/releases/1/zip",
                    "filelist": "http://localhost:8000/api/releases/1/filelist/",
                    "installations": "http://localhost:8000/api/releases/1/installations/",
                    "vulnerabilities": "http://localhost:8000/api/releases/1/vulnerabilities/"
                }
        return response(200, content, headers=RequestBinds.default_headers, request=request)

    @staticmethod
    @urlmatch(scheme=mock_scheme, netloc=mock_netloc, path=mock_basepath + "releases/1/zip/")
    def mock_sourcecode_zip(url, request):
        pass # ToDo

    @staticmethod
    @urlmatch(scheme=mock_scheme, netloc=mock_netloc, path=mock_basepath + "releases/1/filelist/")
    def mock_sourcefile_list(url, request):
        return RequestBinds.__sourcefile_list(url=url, request=request)

    @staticmethod
    @urlmatch(scheme=mock_scheme, netloc=mock_netloc, path=mock_basepath + "releases/1/filelist/", query="page=2")
    def mock_sourcefile_list_page2(url, request):
        return RequestBinds.__sourcefile_list_page2(url=url, request=request)

    @staticmethod
    @urlmatch(scheme=mock_scheme, netloc=mock_netloc, path=mock_basepath + "releases/1/vulnerabilities/")
    def mock_release_vulnerabilities(url, request):
        return RequestBinds.__vulnerability_list(url=url, request=request)

    @staticmethod
    @urlmatch(scheme=mock_scheme, netloc=mock_netloc, path=mock_basepath + "releases/1/vulnerabilities/", query="page=2")
    def mock_release_vulnerabilities_page2(url, request):
        return RequestBinds.__vulnerability_list_page2(url=url, request=request)

    @staticmethod
    @urlmatch(scheme=mock_scheme, netloc=mock_netloc, path=mock_basepath + "releases/1/installations/")
    def mock_release_installations(url, request):
        content = {
            "count": 4,
            "next": url.geturl() + "?page=2",
            "previous": None,
            "results": [
                {
                    "observed_day": "2020-08-01",
                    "count": 24,
                    "installations_for_day": "http://localhost:8000/api/installations/2020-08-01/"
                },
                {
                    "observed_day": "2020-08-02",
                    "count": 23,
                    "installations_for_day": "http://localhost:8000/api/installations/2020-08-01/"
                },
            ]
        }
        return response(200, content, headers=RequestBinds.default_headers, request=request)

    @staticmethod
    @urlmatch(scheme=mock_scheme, netloc=mock_netloc, path=mock_basepath + "releases/1/installations/", query="page=2")
    def mock_release_installations_page2(url, request):
        content = {
            "count": 4,
            "next": None,
            "previous": url.geturl(),
            "results": [
                {
                    "observed_day": "2020-08-01",
                    "count": 24,
                    "installations_for_day": "http://localhost:8000/api/installations/2020-08-01/"
                },
                {
                    "observed_day": "2020-08-01",
                    "count": 23,
                    "installations_for_day": "http://weasel.cs.uni-bonn.de/api/installations/2020-08-01/"
                },
            ]
        }
        return response(200, content, headers=RequestBinds.default_headers, request=request)

    @staticmethod
    @urlmatch(scheme=mock_scheme, netloc=mock_netloc, path=mock_basepath + "releases/WordPress/")
    def mock_release_tech_list(url, request):
        return RequestBinds.mock_release_list(url, request)

    @staticmethod
    @urlmatch(scheme=mock_scheme, netloc=mock_netloc, path=mock_basepath + "releases/WordPress/", query="page=2")
    def mock_release_tech_list_page2(url, request):
        return RequestBinds.mock_release_list_page2(url, request)

    @staticmethod
    @urlmatch(scheme=mock_scheme, netloc=mock_netloc, path=mock_basepath + "technologies/")
    def mock_technology_list(url, request):
        content = [
            {
                "primary_key": 1,
                "tech_name": "WordPress",
                "releases": "http://localhost:8000/api/releases/WordPress/",
                "updates": "http://localhost:8000/api/technologies/1/updates/"
            },
            {
                "primary_key": 2,
                "tech_name": "Nextcloud",
                "releases": "http://localhost:8000/api/releases/Nextcloud/",
                "updates": "http://localhost:8000/api/technologies/2/updates/"
            },
            {
                "primary_key": 3,
                "tech_name": "Mediawiki",
                "releases": "http://localhost:8000/api/releases/Mediawiki/",
                "updates": "http://localhost:8000/api/technologies/3/updates/"
            },
        ]
        return response(200, content, headers=RequestBinds.default_headers, request=request)

    @staticmethod
    @urlmatch(scheme=mock_scheme, netloc=mock_netloc, path=mock_basepath + "file/md5/abc/")

    def mock_sourcefile_byhash(url, request):
        return RequestBinds.__release_sourcefile_list(url=url, request=request)

    @staticmethod
    @urlmatch(scheme=mock_scheme, netloc=mock_netloc, path=mock_basepath + "file/md5/abc/", query="page=2")
    def mock_sourcefile_byhash_page2(url, request):
        return RequestBinds.__release_sourcefile_list_page2(url=url, request=request)

    @staticmethod
    @urlmatch(scheme=mock_scheme, netloc=mock_netloc, path=mock_basepath + "file/1/")
    def mock_sourcefile_detail(url, request):
        content = {
            "url": "http://localhost:8000/api/file/1/",
            "primary_key": 1,
            "fully_qualified_name": "/wp-admin/css/colors/midnight/colors-rtl.css",
            "file": "http://localhost:8000/api/file/1/download/",
            "hash_sha1": "f31a21eabb4bd7e742eb6ee8e6e82e3f06254ca0",
            "hash_sha256": "3b395cab2ff9f80c6a66f24a89ba50562ba3c4f5dcb539da33c1feb192ed1ab4",
            "hash_md5": "58dae4b75a6f54d583e36870591fbc46",
            "hash_ssdeep": "384:LNjNMdUpn/+iKBZbwWS+lUKrVK2/6/GcrGVhZCwBTx:qiKl5lvZ9",
            "releases": "http://localhost:8000/api/file/1/releases/"
        }
        return response(200, content, headers=RequestBinds.default_headers, request=request)

    @staticmethod
    @urlmatch(scheme=mock_scheme, netloc=mock_netloc, path=mock_basepath + "file/1/releases/")
    def mock_sourcefile_releases(url, request):
        return RequestBinds.__release_sourcefile_list(url, request)

    @staticmethod
    @urlmatch(scheme=mock_scheme, netloc=mock_netloc, path=mock_basepath + "file/1/releases/", query="page=2")
    def mock_sourcefile_releases_page2(url, request):
        return RequestBinds.__release_sourcefile_list_page2(url, request)

    @staticmethod
    @urlmatch(scheme=mock_scheme, netloc=mock_netloc, path=mock_basepath + "vulnerability/")
    def mock_vulnerability_list(url, request):
        return RequestBinds.__vulnerability_list(url, request)

    @staticmethod
    @urlmatch(scheme=mock_scheme, netloc=mock_netloc, path=mock_basepath + "vulnerability/", query="page=2")
    def mock_vulnerability_list_page2(url, request):
        return RequestBinds.__vulnerability_list_page2(url, request)

    @staticmethod
    @urlmatch(scheme=mock_scheme, netloc=mock_netloc, path=mock_basepath + "vulnerability/1/")
    def mock_vulnerability_detail(url, request):
        content = {
                    "url": "http://localhost:8000/api/vulnerability/1/",
                    "primary_key": 1,
                    "CVE_number": "CVE-2019-8331",
                    "publish_date": "2019-02-20",
                    "last_modified": "2019-06-11T18:29:00Z",
                    "title": None,
                    "type": None,
                    "references": None,
                    "fishy": None,
                    "found_in": "http://localhost:8000/api/vulnerability/1/found_in/",
                    "fixed_by": "http://localhost:8000/api/vulnerability/1/fixed_by/"
                }
        return response(200, content, headers=RequestBinds.default_headers, request=request)

    @staticmethod
    @urlmatch(scheme=mock_scheme, netloc=mock_netloc, path=mock_basepath + "vulnerability/1/found_in/")
    def mock_vulnerability_found_in(url, request):
        return RequestBinds.__release_list(url, request)

    @staticmethod
    @urlmatch(scheme=mock_scheme, netloc=mock_netloc, path=mock_basepath + "vulnerability/1/found_in/", query="page=2")
    def mock_vulnerability_found_in_page2(url, request):
        return RequestBinds.__release_list_page2(url, request)

    @staticmethod
    @urlmatch(scheme=mock_scheme, netloc=mock_netloc, path=mock_basepath + "vulnerability/1/fixed_by/")
    def mock_vulnerability_fixed_by(url, request):
        return RequestBinds.__release_list(url, request)

    @staticmethod
    @urlmatch(scheme=mock_scheme, netloc=mock_netloc, path=mock_basepath + "vulnerability/1/fixed_by/", query="page=2")
    def mock_vulnerability_fixed_by_page2(url, request):
        return RequestBinds.__release_list_page2(url, request)

    @staticmethod
    @urlmatch(scheme=mock_scheme, netloc=mock_netloc, path=mock_basepath + "installations/")
    def mock_installations_day_list(url, request):
        content = {
            "count": 2,
            "next": url.geturl() + "?page=2",
            "previous": None,
            "results": [
                {
                    "date": "2020-08-01",
                    "results": "http://localhost:8000/api/installations/2020-08-01/"
                },
            ]
        }
        return response(200, content, headers=RequestBinds.default_headers, request=request)

    @staticmethod
    @urlmatch(scheme=mock_scheme, netloc=mock_netloc, path=mock_basepath + "installations/", query="page=2")
    def mock_installations_day_list_page2(url, request):
        content = {
            "count": 2,
            "next": None,
            "previous": url.geturl(),
            "results": [
                {
                    "date": "2020-08-01",
                    "results": "http://localhost:8000/api/installations/2020-08-01/"
                },
            ]
        }
        return response(200, content, headers=RequestBinds.default_headers, request=request)

    @staticmethod
    @urlmatch(scheme=mock_scheme, netloc=mock_netloc, path=mock_basepath + "installations/2020-08-01/")
    def mock_installations_list(url, request):
        content = {
            "count": 2,
            "next": url.geturl() + "?page=2",
            "previous": None,
            "results": [
                {
                    "release": "http://localhost:8000/api/releases/1/",
                    "release_name": "WordPress 5.3.0",
                    "count": 7,
                    "installations_for_release": "http://localhost:8000/api/releases/1/installations/"
                },
            ]
        }
        return response(200, content, headers=RequestBinds.default_headers, request=request)

    @staticmethod
    @urlmatch(scheme=mock_scheme, netloc=mock_netloc, path=mock_basepath + "installations/2020-08-01/", query="page=2")
    def mock_installations_list_page2(url, request):
        content = {
            "count": 2,
            "next": None,
            "previous": url.geturl(),
            "results": [
                {
                    "release": "http://localhost:8000/api/releases/1/",
                    "release_name": "WordPress 5.3.0",
                    "count": 7,
                    "installations_for_release": "http://localhost:8000/api/releases/1/installations/"
                },
            ]
        }
        return response(200, content, headers=RequestBinds.default_headers, request=request)

    @staticmethod
    @urlmatch(scheme=mock_scheme, netloc=mock_netloc, path="/")
    def mock_notfound(url, request):
        content = {
            "detail": "Not found."
        }
        return response(404, content, headers=RequestBinds.default_headers, request=request)

    @staticmethod
    @urlmatch(scheme=mock_scheme, netloc=mock_netloc, path="/")
    def mock_paginated_notfound(url, request):
        content = {
            "count": 0,
            "next": None,
            "previous": None,
            "results": []
        }
        return response(200, content, headers=RequestBinds.default_headers, request=request)

    @staticmethod
    def __release_list(url, request):
        content = {
            "count": 4,
            "next": url.geturl() + "?page=2",
            "previous": None,
            "results": [
                {
                    "url": "http://localhost:8000/api/releases/1/",
                    "primary_key": 1,
                    "technology": "WordPress",
                    "version": "5.3.0",
                    "prev_release": None,
                    "published": "2005-05-10T00:00:00Z",
                    "zipfile": "http://localhost:8000/api/releases/1/zip",
                    "filelist": "http://localhost:8000/api/releases/1/filelist/",
                    "installations": "http://localhost:8000/api/releases/1/installations/",
                    "vulnerabilities": "http://localhost:8000/api/releases/1/vulnerabilities/"
                },
                {
                    "url": "http://localhost:8000/api/releases/1/",
                    "primary_key": 1,
                    "technology": "WordPress",
                    "version": "5.3.0",
                    "prev_release": None,
                    "published": "2005-05-10T00:00:00Z",
                    "zipfile": "http://localhost:8000/api/releases/1/zip",
                    "filelist": "http://localhost:8000/api/releases/1/filelist/",
                    "installations": "http://localhost:8000/api/releases/1/installations/",
                    "vulnerabilities": "http://localhost:8000/api/releases/1/vulnerabilities/"
                },
            ]
        }
        return response(200, content, headers=RequestBinds.default_headers, request=request)

    @staticmethod
    def __release_list_page2(url, request):
        content = {
            "count": 4,
            "next": None,
            "previous": url.geturl(),
            "results": [
                {
                    "url": "http://localhost:8000/api/releases/1/",
                    "primary_key": 1,
                    "technology": "WordPress",
                    "version": "5.3.0",
                    "prev_release": None,
                    "published": "2005-05-10T00:00:00Z",
                    "zipfile": "http://localhost:8000/api/releases/1/zip",
                    "filelist": "http://localhost:8000/api/releases/1/filelist/",
                    "installations": "http://localhost:8000/api/releases/1/installations/",
                    "vulnerabilities": "http://localhost:8000/api/releases/1/vulnerabilities/"
                },
                {
                    "url": "http://localhost:8000/api/releases/1/",
                    "primary_key": 1,
                    "technology": "WordPress",
                    "version": "5.3.0",
                    "prev_release": None,
                    "published": "2005-05-10T00:00:00Z",
                    "zipfile": "http://localhost:8000/api/releases/1/zip",
                    "filelist": "http://localhost:8000/api/releases/1/filelist/",
                    "installations": "http://localhost:8000/api/releases/1/installations/",
                    "vulnerabilities": "http://localhost:8000/api/releases/1/vulnerabilities/"
                },
            ]
        }
        return response(200, content, headers=RequestBinds.default_headers, request=request)

    @staticmethod
    def __sourcefile_list(url, request):
        content = {
            "count": 2,
            "next": url.geturl() + "?page=2",
            "previous": None,
            "results": [
                {
                    "url": "http://localhost:8000/api/file/1/",
                    "primary_key": 1,
                    "fully_qualified_name": "/wp-admin/css/colors/midnight/colors-rtl.css",
                    "file": "http://localhost:8000/api/file/1/download/",
                    "hash_sha1": "f31a21eabb4bd7e742eb6ee8e6e82e3f06254ca0",
                    "hash_sha256": "3b395cab2ff9f80c6a66f24a89ba50562ba3c4f5dcb539da33c1feb192ed1ab4",
                    "hash_md5": "58dae4b75a6f54d583e36870591fbc46",
                    "hash_ssdeep": "384:LNjNMdUpn/+iKBZbwWS+lUKrVK2/6/GcrGVhZCwBTx:qiKl5lvZ9",
                    "releases": "http://localhost:8000/api/file/1/releases/"
                }
            ]
        }
        return response(200, content, headers=RequestBinds.default_headers, request=request)

    @staticmethod
    def __sourcefile_list_page2(url, request):
        content = {
            "count": 2,
            "next": None,
            "previous": url.geturl(),
            "results": [
                {
                    "url": "http://localhost:8000/api/file/1/",
                    "primary_key": 1,
                    "fully_qualified_name": "/wp-admin/css/colors/midnight/colors-rtl.css",
                    "file": "http://localhost:8000/api/file/1/download/",
                    "hash_sha1": "f31a21eabb4bd7e742eb6ee8e6e82e3f06254ca0",
                    "hash_sha256": "3b395cab2ff9f80c6a66f24a89ba50562ba3c4f5dcb539da33c1feb192ed1ab4",
                    "hash_md5": "58dae4b75a6f54d583e36870591fbc46",
                    "hash_ssdeep": "384:LNjNMdUpn/+iKBZbwWS+lUKrVK2/6/GcrGVhZCwBTx:qiKl5lvZ9",
                    "releases": "http://localhost:8000/api/file/1/releases/"
                }
            ]
        }
        return response(200, content, headers=RequestBinds.default_headers, request=request)

    @staticmethod
    def __release_sourcefile_list(url, request):
        content = {
            "count": 2,
            "next": url.geturl() + "?page=2",
            "previous": None,
            "results": [
                {
                    "release": "http://localhost:8000/api/releases/1/",
                    "sourcefile": "http://localhost:8000/api/file/1/",
                    "fully_qualified_name": "/index.php"
                }
            ]
        }
        return response(200, content, headers=RequestBinds.default_headers, request=request)

    @staticmethod
    def __release_sourcefile_list_page2(url, request):
        content = {
            "count": 2,
            "next": None,
            "previous": url.geturl(),
            "results": [
                {
                    "release": "http://localhost:8000/api/releases/1/",
                    "sourcefile": "http://localhost:8000/api/file/1/",
                    "fully_qualified_name": "/index.php"
                }
            ]
        }
        return response(200, content, headers=RequestBinds.default_headers, request=request)

    @staticmethod
    def __vulnerability_list(url, request):
        content = {
            "count": 2,
            "next": url.geturl() + "?page=2",
            "previous": None,
            "results": [
                {
                    "url": "http://localhost:8000/api/vulnerability/1/",
                    "primary_key": 1,
                    "CVE_number": "CVE-2019-8331",
                    "publish_date": "2019-02-20",
                    "last_modified": "2019-06-11T18:29:00Z",
                    "title": None,
                    "type": None,
                    "references": None,
                    "fishy": None,
                    "found_in": "http://localhost:8000/api/vulnerability/1/found_in/",
                    "fixed_by": "http://localhost:8000/api/vulnerability/1/fixed_by/"
                },
            ]
        }
        return response(200, content, headers=RequestBinds.default_headers, request=request)

    @staticmethod
    def __vulnerability_list_page2(url, request):
        content = {
            "count": 2,
            "next": None,
            "previous": url.geturl(),
            "results": [
                {
                    "url": "http://localhost:8000/api/vulnerability/1/",
                    "primary_key": 1,
                    "CVE_number": "CVE-2019-8331",
                    "publish_date": "2019-02-20",
                    "last_modified": "2019-06-11T18:29:00Z",
                    "title": None,
                    "type": None,
                    "references": None,
                    "fishy": None,
                    "found_in": "http://localhost:8000/api/vulnerability/1/found_in/",
                    "fixed_by": "http://localhost:8000/api/vulnerability/1/fixed_by/"
                },
            ]
        }
        return response(200, content, headers=RequestBinds.default_headers, request=request)
