import unittest
from weasel_client import APIClient, DateStatistics
from httmock import all_requests, urlmatch, HTTMock, response
from .request_binds import RequestBinds
import dateutil.parser
from datetime import datetime, date


class DateStatisticsTestCase(unittest.TestCase):
	_client: APIClient
	_datestat: DateStatistics

	def setUp(self) -> None:
		with HTTMock(RequestBinds.mock_installations_day_list_page2, RequestBinds.mock_installations_day_list,
		             RequestBinds.mock_root):
			self._client = APIClient(token="abcdefg", url="http://localhost:8000/api/")
			self._datestat = DateStatistics.from_date(api_client=self._client, day=date.fromisoformat("2020-08-01"))

	def test_results(self):
		with HTTMock(RequestBinds.mock_installations_list_page2, RequestBinds.mock_installations_list):
			for rc in self._datestat.results():
				self.assertIsNotNone(rc)


if __name__ == '__main__':
	unittest.main()
