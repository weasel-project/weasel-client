from pprint import pprint
import unittest
import requests

from httmock import all_requests, urlmatch, HTTMock, response
from weasel_client import APIClient
from .request_binds import RequestBinds


class APIClientTestCase(unittest.TestCase):
	def test_connection_wrongtoken(self):
		with HTTMock(RequestBinds.mock_connection_wrongtoken):
			api_client = self._get_client(token="blubdiblub")
			self.assertFalse(api_client.connected())

	def test_connection_working(self):
		with HTTMock(RequestBinds.mock_root):
			api_client = self._get_client()
			self.assertTrue(api_client.connected())

	def test_release_function(self):
		with HTTMock(RequestBinds.mock_release_detail, RequestBinds.mock_technology_list, RequestBinds.mock_root):
			api_client = self._get_client()
			release = api_client.release(primary_key=1)
			self.assertIsNotNone(release)

	def test_technologies_function(self):
		with HTTMock(RequestBinds.mock_technology_list, RequestBinds.mock_root):
			api_client = self._get_client()
			for technology in api_client.technologies():
				self.assertIsNotNone(technology)

	def test_vulnerabilities_function(self):
		with HTTMock(RequestBinds.mock_vulnerability_list_page2, RequestBinds.mock_vulnerability_list,
		             RequestBinds.mock_root):
			api_client = self._get_client()
			for vulnerability in api_client.vulnerabilities():
				self.assertIsNotNone(vulnerability)

	def test_installations_function(self):
		with HTTMock(RequestBinds.mock_installations_day_list_page2, RequestBinds.mock_installations_day_list,
		             RequestBinds.mock_root):
			api_client = self._get_client()
			for day in api_client.installations():
				self.assertIsNotNone(day)

	def test_technology_function(self):
		with HTTMock(RequestBinds.mock_technology_list, RequestBinds.mock_root):
			api_client = self._get_client()
			tech = api_client.technology("WordPress")
			self.assertIsNotNone(tech)

	def test_technology_nonexistent(self):
		with HTTMock(RequestBinds.mock_technology_list, RequestBinds.mock_root):
			api_client = self._get_client()
			tech = api_client.technology("Warpantrieb")
			self.assertIsNone(tech)

	def test_release(self):
		with HTTMock(RequestBinds.mock_release_detail, RequestBinds.mock_technology_list, RequestBinds.mock_root):
			api_client = self._get_client()
			release = api_client.release(1)
			self.assertIsNotNone(release)

	def test_release_nonexistent(self):
		with HTTMock(RequestBinds.mock_notfound):
			api_client = self._get_client()
			release = api_client.release(1337)
			self.assertIsNone(release)

	def test_release_list(self):
		with HTTMock(RequestBinds.mock_release_list_page2, RequestBinds.mock_release_list,
		             RequestBinds.mock_technology_list, RequestBinds.mock_root):
			api_client = self._get_client()
			for release in api_client.release_list():
				self.assertIsNotNone(release)

	def test_release_detail(self):
		with HTTMock(RequestBinds.mock_release_detail, RequestBinds.mock_technology_list, RequestBinds.mock_root):
			api_client = self._get_client()
			release = api_client.release_detail(primary_key=1)
			self.assertIsNotNone(release)

	def test_release_detail_nonexistent(self):
		with HTTMock(RequestBinds.mock_notfound):
			api_client = self._get_client()
			release = api_client.release_detail(primary_key=1337)
			self.assertIsNone(release)

	def test_sourcefile_list(self):
		with HTTMock(RequestBinds.mock_sourcefile_list_page2, RequestBinds.mock_sourcefile_list,
		             RequestBinds.mock_root):
			api_client = self._get_client()
			for sourcefile in api_client.sourcefile_list(primary_key=1):
				self.assertIsNotNone(sourcefile)

	def test_release_vulnerabilities(self):
		with HTTMock(RequestBinds.mock_release_vulnerabilities_page2, RequestBinds.mock_release_vulnerabilities):
			api_client = self._get_client()
			for vulnerability in api_client.release_vulnerabilities(primary_key=1):
				self.assertIsNotNone(vulnerability)

	def test_release_installations(self):
		with HTTMock(RequestBinds.mock_release_installations_page2, RequestBinds.mock_release_installations):
			api_client = self._get_client()
			for installation in api_client.release_installations(primary_key=1):
				self.assertIsNotNone(installation)

	def test_release_tech_list(self):
		with HTTMock(RequestBinds.mock_release_tech_list_page2, RequestBinds.mock_release_tech_list,
		             RequestBinds.mock_technology_list):
			api_client = self._get_client()
			for release in api_client.release_tech_list("WordPress"):
				self.assertIsNotNone(release)

	def test_technology_list(self):
		with HTTMock(RequestBinds.mock_technology_list):
			api_client = self._get_client()
			for tech in api_client.technology_list():
				self.assertIsNotNone(tech)

	def test_sourcefile_byhash(self):
		with HTTMock(RequestBinds.mock_sourcefile_byhash_page2, RequestBinds.mock_sourcefile_byhash):
			api_client = self._get_client()
			for file in api_client.sourcefile_byhash("md5", "abc"):
				self.assertIsNotNone(file)

	def test_sourcefile_detail(self):
		with HTTMock(RequestBinds.mock_sourcefile_detail):
			api_client = self._get_client()
			file = api_client.sourcefile_detail(primary_key=1)
			self.assertIsNotNone(file)

	def test_sourcefile_detail_nonexistent(self):
		with HTTMock(RequestBinds.mock_notfound):
			api_client = self._get_client()
			file = api_client.sourcefile_detail(primary_key=1337)
			self.assertIsNone(file)

	def test_sourcefile_releases(self):
		with HTTMock(RequestBinds.mock_sourcefile_releases_page2, RequestBinds.mock_sourcefile_releases):
			api_client = self._get_client()
			for file in api_client.sourcefile_releases(primary_key=1):
				self.assertIsNotNone(file)

	def test_vulnerability_list(self):
		with HTTMock(RequestBinds.mock_vulnerability_list_page2, RequestBinds.mock_vulnerability_list):
			api_client = self._get_client()
			for vuln in api_client.vulnerability_list():
				self.assertIsNotNone(vuln)

	def test_vulnerability_detail(self):
		with HTTMock(RequestBinds.mock_vulnerability_detail):
			api_client = self._get_client()
			vuln = api_client.vulnerability_detail(primary_key=1)
			self.assertIsNotNone(vuln)

	def test_vulnerability_detail_nonexistent(self):
		with HTTMock(RequestBinds.mock_notfound):
			api_client = self._get_client()
			vuln = api_client.vulnerability_detail(primary_key=1337)
			self.assertIsNone(vuln)

	def test_vulnerability_found_in(self):
		with HTTMock(RequestBinds.mock_vulnerability_found_in_page2, RequestBinds.mock_vulnerability_found_in,
		             RequestBinds.mock_technologies):
			api_client = self._get_client()
			for release in api_client.vulnerability_found_in(primary_key=1):
				self.assertIsNotNone(release)

	def test_vulnerability_fixed_by(self):
		with HTTMock(RequestBinds.mock_vulnerability_fixed_by_page2, RequestBinds.mock_vulnerability_fixed_by,
		             RequestBinds.mock_technologies):
			api_client = self._get_client()
			for release in api_client.vulnerability_fixed_by(primary_key=1):
				self.assertIsNotNone(release)

	def test_installations_day_list(self):
		with HTTMock(RequestBinds.mock_installations_day_list_page2, RequestBinds.mock_installations_day_list):
			api_client = self._get_client()
			for day in api_client.installations_day_list():
				self.assertIsNotNone(day)

	def test_installations_list(self):
		with HTTMock(RequestBinds.mock_installations_list_page2, RequestBinds.mock_installations_list):
			api_client = self._get_client()
			for rc in api_client.installations_list(date="2020-08-01"):
				self.assertIsNotNone(rc)

	def test_tech_generator(self):
		with HTTMock(RequestBinds.mock_release_tech_list_page2, RequestBinds.mock_release_tech_list,
		             RequestBinds.mock_technologies, RequestBinds.mock_root):
			api_client = self._get_client()
			for release in api_client.technology("WordPress").releases():
				self.assertIsNotNone(release)

	def test_tech_release_generator(self):
		with HTTMock(RequestBinds.mock_sourcefile_list_page2, RequestBinds.mock_sourcefile_list,
		             RequestBinds.mock_release_tech_list_page2, RequestBinds.mock_release_tech_list,
		             RequestBinds.mock_technology_list, RequestBinds.mock_root):
			api_client = self._get_client()
			for release in api_client.technology("WordPress").releases():
				for file in release.filelist():
					self.assertIsNotNone(file)

	def _get_client(self, token="abcdefg"):
		APIClient.destruct()
		return APIClient(token=token, url="http://localhost:8000/api/")


if __name__ == '__main__':
	unittest.main()
