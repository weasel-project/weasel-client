import unittest
from weasel_client import APIClient, Technology
from httmock import all_requests, urlmatch, HTTMock, response
from .request_binds import RequestBinds


class TechnologyTestCase(unittest.TestCase):
	_client: APIClient
	_technology: Technology

	def setUp(self) -> None:
		with HTTMock(RequestBinds.mock_technology_list, RequestBinds.mock_root):
			self._client = APIClient(token="abcdefg", url="http://localhost:8000/api/")
			self._technology = Technology.from_name(api_client=self._client, name="WordPress")

	def test_Technology_from_name(self):
		with HTTMock(RequestBinds.mock_technology_list):
			tech = Technology.from_name(api_client=self._client, name="WordPress")
			self.assertIsNotNone(tech)

	def test_Technology_invalid_tech(self):
		with HTTMock(RequestBinds.mock_technology_list):
			tech = Technology.from_name(api_client=self._client, name="Invalid tech name is invalid")
			self.assertIsNone(tech)

	def test_releases(self):
		with HTTMock(RequestBinds.mock_technology_list, RequestBinds.mock_release_tech_list_page2,
		             RequestBinds.mock_release_tech_list):
			for release in self._technology.releases():
				self.assertIsNotNone(release)


if __name__ == '__main__':
	unittest.main()
