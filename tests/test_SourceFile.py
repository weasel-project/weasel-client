import unittest
from weasel_client import APIClient, SourceFile
from httmock import all_requests, urlmatch, HTTMock, response
from .request_binds import RequestBinds


class SourceFileTestCase(unittest.TestCase):
	_client: APIClient
	_file: SourceFile

	def setUp(self) -> None:
		with HTTMock(RequestBinds.mock_sourcefile_detail, RequestBinds.mock_root):
			self._client = APIClient(token="abcdefg", url="http://localhost:8000/api/")
			self._file = SourceFile.by_id(api_client=self._client, primary_key=1)

	def test_SourceFile_by_id(self):
		with HTTMock(RequestBinds.mock_sourcefile_detail):
			self.assertIsNotNone(SourceFile.by_id(api_client=self._client, primary_key=1))

	def test_SourceFile_by_hash(self):
		with HTTMock(RequestBinds.mock_sourcefile_byhash_page2, RequestBinds.mock_sourcefile_byhash):
			for file in SourceFile.by_hash(api_client=self._client, hash_type="md5", hash_content="abc"):
				self.assertIsNotNone(file)

	def test_releases(self):
		with HTTMock(RequestBinds.mock_sourcefile_releases_page2, RequestBinds.mock_sourcefile_releases):
			for release in self._file.releases():
				self.assertIsNotNone(release)

if __name__ == '__main__':
	unittest.main()
